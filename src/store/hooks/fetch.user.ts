import { UserAction,UserActionTypes } from '../reducers/user.reducer';
import { Dispatch, useEffect } from 'react';
import axios from "../../index";
export const fetchUser = (token:any)=>{
    return async(dispatch:Dispatch<UserAction>)=>{
      const resp  = await axios.get('get-user');
      const user = resp.data;
      dispatch({type:UserActionTypes.ADD_USER,payload:user})
    }
}