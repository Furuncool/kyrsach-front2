
import axios from "../../index";
import React, { FormEvent, useState } from "react";
import { useNavigate } from "react-router";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchUser } from "../../store/hooks/fetch.user";


const Login = () => {
  const navigate = useNavigate();
  const dispatch:any = useDispatch();
  const [form, setForm] = useState({ email: '', password: '' });
  async function fetchForm(e: FormEvent) {
    e.preventDefault();
    try {
      const { data } = await axios.post('/login', form);
      localStorage.setItem('token', data.accessToken);
      axios.defaults.headers.common['Authorization'] = `Bearer ${data.accessToken}`;
      dispatch(fetchUser(data.accessToken));
      navigate('/');
    } catch (e) {

    }
  }
  return (
    <form className="auth__form" onSubmit={fetchForm}>
      <div className="auth__input-block">
        <label htmlFor="login">Логин</label>
        <input
          value={form.email}
          onChange={e => setForm({ ...form, email: e.target.value })}
          id="login"
          type="text"
          placeholder="Логин"
          required
        />
      </div>
      <div className="auth__input-block">
        <label htmlFor="pass">Пароль</label>
        <input
          value={form.password}
          onChange={e => setForm({ ...form, password: e.target.value })}
          id="pass"
          type="password"
          placeholder="Пароль"
          required
        />
      </div>
      <div className="auth__subform-block">
        <button>Войти</button>
        <Link to='/registration'>Зарегистрироваться</Link>
      </div>
    </form>
  );
};

export default Login;