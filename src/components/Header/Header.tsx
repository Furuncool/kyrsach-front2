
import { useLocation } from 'react-router';
import { Link } from 'react-router-dom';
import logo from '../../static/img/header/heder.png';
import { useSelector } from 'react-redux';
import { useTypesSelector } from '../../store/hooks/user.type.selector';
import { Dispatch, useEffect } from 'react';
import { UserAction,UserActionTypes } from '../../store/reducers/user.reducer';
import { useDispatch } from 'react-redux';
import { stat } from 'fs';
import { render } from '@testing-library/react';
import axios from '../../index';
import { useNavigate } from "react-router-dom";

export function Header(){
    const path = useLocation().pathname;
    const navigation = useNavigate();
    const dispatch = useDispatch();
    const state = useTypesSelector(state=>state.user);
    const token = localStorage.getItem('token');
    const logout = () =>{
      localStorage.removeItem('token');
      axios.defaults.headers.common['Authorization'] = '';
      dispatch({type:UserActionTypes.ADD_USER,payload:{}})
      navigation('/');
    }
    if(path=='/login' || path=="/registration"){
      return null;
    }

  




  return (
    <header className="header">
    <div className="logo"><Link to='/'><img src={logo} alt="logo"/></Link></div>
    <nav className="header__nav">
      {Object.keys(state.user).length >0 ? (
        
        <Link to="/post-create">Добавить новость</Link>
      )  : null
      }
      {
      Object.keys(state.user).length>0?(  
      <button type='button' onClick={logout} >Выйти</button>
      ):<Link to='/login'>Войти</Link>
      }
    </nav>
  </header> 
  )
}



export default Header;



